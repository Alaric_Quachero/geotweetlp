package com.sophiaantipolis.quacheton.geotweetlp;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.twitter.sdk.android.core.models.Tweet;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class GeolocalisationFragment extends Fragment implements OnMapReadyCallback {

    private final LatLng CASINO_BIOT = new LatLng(43.6178091, 7.075244600000019);
    private MapView mapView;
    private View vue;
    private Button buttonAfficherLocalisation;
    GoogleMap googleMap;

    public GeolocalisationFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vue = inflater.inflate(R.layout.fragment_geolocalisation, container, false);
        Button button = (Button) vue.findViewById(R.id.buttonAfficherInfos);

        buttonAfficherLocalisation = (Button) vue.findViewById(R.id.buttonAfficherInfos);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(getContext(), "Nombre de tweets trouvés: "+Tweets.getTweets().size(), Toast.LENGTH_SHORT).show();
                afficherTweets(googleMap);
            }
        });


        return vue;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mapView = (MapView) view.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);
    }

    public void activeLocalisation(GoogleMap map) {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        } else {
            Toast.makeText(getContext(), "Localisation indisponible", Toast.LENGTH_SHORT).show();
        }
    }

    private void configurationMap(GoogleMap mMap) {
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        mMap.getUiSettings().setMapToolbarEnabled(false);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(CASINO_BIOT, 15));

        activeLocalisation(mMap);

        mMap.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);
    }

    private void afficherTweets(GoogleMap mMap){

        mMap.clear();

        for (Tweet tweet:Tweets.getTweets()) {
            if (tweet.coordinates != null) {
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(tweet.coordinates.getLatitude(), tweet.coordinates.getLongitude()))
                        .title(tweet.user.name)
                        .snippet(tweet.text)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        configurationMap(map);
        googleMap = map;
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}