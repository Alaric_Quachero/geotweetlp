package com.sophiaantipolis.quacheton.geotweetlp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.twitter.sdk.android.core.models.Tweet;

import java.util.ArrayList;


public class CollecterFragment extends Fragment {
    private View vue;
    private EditText editTextCollecter;
    private ListView mListView;
    private ArrayList<String> texte = new ArrayList<>();
    //private ArrayAdapter<String> adapter;

    public CollecterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vue = inflater.inflate(R.layout.fragment_collecter, container, false);

        final Button button = (Button) vue.findViewById(R.id.buttonCollecterTweets);
        final Button buttonAfficher = (Button) vue.findViewById(R.id.buttonAfficher);
        editTextCollecter = (EditText) vue.findViewById(R.id.editTextCollecter);
        mListView = (ListView) vue.findViewById(R.id.listTweets);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Tweets.queryTweetsByHashTag(editTextCollecter.getText().toString(), 20);
            }
        });
        buttonAfficher.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                for (Tweet tweet:Tweets.getTweets()) {
                    texte.add(tweet.user.name + " :\n"+ tweet.text);
                }
                Toast.makeText(getContext(), "Nombre de tweets: " + Tweets.getTweets().size(), Toast.LENGTH_SHORT).show();

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_list_item_1, texte);
                mListView.setAdapter(adapter);
            }
        });
        return vue;
    }
}